# Angular 2 application with ngrx store


To get started, run the commands below.

```
$ cd TodoApp
$ npm install
$ typings install
$ npm start
```

Then navigate to [http://localhost:3001](http://localhost:3001) in your browser.

Author - SarunUK
