import {Item} from './item.model';

export interface Gadget {
  items: Item[];
};
