import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {AppStore} from '../models/appstore.model';
import {Gadget} from "../models/gadget.model.ts";
import {Item} from "../models/item.model";

import * as Rx from 'rxjs/Rx';

@Injectable()
export class GadgetService {
  gadget: Observable<Gadget>;
  items: Observable<Array<Item>>;

  constructor(private store: Store<AppStore>) {
    this.gadget = Rx.Observable.combineLatest(
      store.select('items'),
      (items: Item[] = []) => {
        return {
          items: [...items]
        }
      });

    this.gadget
      .subscribe(c => console.log('GadgetService.gadget', c));
  }
};
